# Adventurer's Code

| Adventurer's Code | 1.0                                         |
| ----------------- | ------------------------------------------- |
| Maintainer        | Allele Dev (allele.dev@gmail.com)           |
| Copyright         | Copyright (C) 2018 Allele Dev               |
| License           | CC Attribution-ShareAlike 4.0 International |

A code of conduct for free companies and communities looking to create
inclusive spaces in the world of
[FFXIV](https://www.finalfantasyxiv.com/).

It is scoped to free companies in particular, but may be adapted to
suit larger spaces, such as a shared, multi-free-company discord or
other collaborative space.

It may also be adapted for use in games with a similar structure to
FFXIV, with some minor change in terminology.

Adapted from the [Contributor
Covenant](https://www.contributor-covenant.org/), an excellent code of
conduct for software projects.

Available for use here: [Adventurer's Code](adventurers-code.md)

## Project Code of Conduct and License

The maintenance of this document is governed by the [Contributor
Covenant](https://www.contributor-covenant.org/version/1/4/code-of-conduct).

It is also available for review [here](CODE_OF_CONDUCT.md), as
CODE_OF_CONDUCT.md.

The work is licensed under the terms of the Creative Common's
Attribution-ShareAlike 4.0 International License.

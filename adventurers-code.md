# Adventurer's Code

## Our Pledge

In the interest of fostering an open and welcoming environment, we as
adventurers pledge to making participation in our community a
harassment-free experience for everyone, regardless of:

* age
* body size
* disability
* ethnicity
* gender identity and expression
* level of experience
* education
* socio-economic status
* nationality
* personal appearance
* race
* religion
* sexual identity and orientation

## Our Standards

Examples of behavior that contributes to creating a positive
environment include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual
  attention or advances

* Trolling, insulting/derogatory comments, and personal or political
  attacks

* Public or private harassment

* Publishing others' private information, such as a physical or
  electronic address, without explicit permission

Additionally, take note of the following:

* we are ace- and non-binary inclusive

* "reverse racism" isn't real and invoking it is at least a warnable
  offense

* "diversity of ideology" is a non-goal; there's no room for bigotry
  here

* freedom of speech never means you're not responsible for being
  awful, and does not afford you protections from disciplinary actions

* folks that will be banned immediately:
  * nazis
  * alt-right
  * TERFs
  * SWERFs

## Our Responsibilities

Community moderators are responsible for clarifying the standards of
acceptable behavior and are expected to take appropriate and fair
corrective action in response to any instances of unacceptable
behavior.

Community moderators have the right and responsibility to:

* mediate discussions
* warn against and discourage undesirable behaviors
* ban - temporarily or permanently - members from all private spaces
  where the community operates for violations of the Adventurer's Code
* escalate offenses to game administrators that fall within their
  jurisdiction, notably, Section 3 of the [User
  Agreement](http://support.na.square-enix.com/rule.php?id=5382&tag=users_en).

This is the basic scope of enforcement detailed by the Adventurer's
Code, and it is up to community moderators to enforce it accordingly.

## Scope

The Adventurer's Code applies both within free company spaces and in
public spaces when an individual is representing the free company or
its community. Examples of representing a free company include:

* shouting in public areas on behalf of the free company
* posting via a social media account associated with the free company
* participating in chat spaces created for community members to
  communicate and collaborate outside of the game (discord, etc.)

Representation of a free company may be further defined and clarified
by community moderators.

## Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior
may be reported by contacting the moderation team at [INSERT EMAIL
ADDRESS]. All complaints will be reviewed and investigated and will
result in a response that is deemed necessary and appropriate to the
circumstances. The community team is obligated to maintain
confidentiality with regard to the reporter of an incident.  Further
details of specific enforcement policies may be posted separately.

Community moderators who do not follow or enforce the Adventurer's
Code in good faith may face temporary or permanent repercussions as
determined by other members of the free company's leadership.

## Attribution

The Adventurer's Code is adapted from the [Contributor
Covenant][homepage], version 1.4, available at
https://www.contributor-covenant.org/version/1/4/code-of-conduct.html

[homepage]: https://www.contributor-covenant.org

The following is a small list of the primary changes made to the
Contributor Covenant to adapt it for use in FFXIV:

* "project" -> "free company"
* "maintainer" -> "moderator"
* some reformatting to aid readability
* removal of reference to professional behavior
* addition of specific behaviors and folks that will not be tolerated
